package userProfile;

import java.util.LinkedList;
import java.util.List;

import products.Product;

public class UserProfileProcessor {

	public List<UserProfile> createAllUsersProfiles(List<LPSPersonalised>lpsPersonaliseds, List<Product>products) {
		List<UserProfile>userProfiles = new LinkedList<UserProfile>();
		UserProfile userProfile ;
		int indexProfile=-1;
		String idOfSearchedProduct;
		Product tempProduct =null;
		List<String> idsOfExpensiveProducts = getIdsOfExpensiveProducts(products);
		for(LPSPersonalised lps : lpsPersonaliseds) {
			userProfile = createUserProfile(lps,products);
			indexProfile = getIndexUserProfile(userProfiles,userProfile);
			if(indexProfile == -1) {
				userProfiles.add(userProfile);
			}else {
				if(lps.getMethodName().equals("addProduct") || lps.getMethodName().equals("deleteProduct") ||lps.getMethodName().equals("updateProduct") ) {
					userProfiles.get(indexProfile).getWritingOperations().add(lps.getMethodName());
				}
				// tout opération nécessite une vérification d'existance don une lecture aussi
				userProfiles.get(indexProfile).getReadingOperations().add(lps.getMethodName());
				
				// traitement pour obtenir les produits les plus chères rechercher
				if(lps.getMethodName().equals("fetchProduct")) {
					idOfSearchedProduct = getIndOfProductFromMsg(lps.getMsg());
					if(idsOfExpensiveProducts.contains(idOfSearchedProduct)) {
						tempProduct = getProductById(idOfSearchedProduct,products);
						userProfiles.get(indexProfile).getMostExpensiveProductsSearched().add(tempProduct);
					}
				}
			}
		}
		return userProfiles;
	}



	private Product getProductById(String idOfProduct,List<Product> products) {
		for (Product product : products) {
			if(product.getID().equals(idOfProduct)) {
				return product;
			}	
		}
		return null;
	}



	private List<String> getIdsOfExpensiveProducts(List<Product> products) {
		List<String>IdsOfExpensiveProducts = new LinkedList<String>();
		for (int i = 0; i < products.size(); i++) {
			// les 3 produits les plus chères sont les premiers
			if(i>=3) {
				break;
			}else {
				IdsOfExpensiveProducts.add(products.get(i).getID());
			}
		}
		return IdsOfExpensiveProducts;
	}



	private String getIndOfProductFromMsg(String msg) {
		String tab[]= msg.split(";");
		return tab[1];
	}



	private int getIndexUserProfile(List<UserProfile> userProfiles, UserProfile userProfile) {
		for (int i = 0; i < userProfiles.size(); i++) {
			if(userProfiles.get(i).getUser().getID().equals(userProfile.getUser().getID())) {
				return i;
			}
		}
		
		return -1;
	}



	private UserProfile createUserProfile(LPSPersonalised lps,List<Product>products) {
		String strUser = lps.getMsg();
		String tab0[]= strUser.split("];");
		String tab[]= tab0[0].split(",");
		String tab2[];
		User user = new User();
		for (int i = 0; i < tab.length; i++) {
			tab2=tab[i].split("=");
			switch (i) {
				case 0: {
					user.setID(tab2[1]);
					break;
				}
				case 1: {
					user.setName(tab2[1]);
					break;
				}
				case 2: {
					user.setAge(tab2[1]);
					break;
				}
				case 3: {
					user.setEmail(tab2[1]);
					break;
				}
				case 4: {
					user.setPassword(tab2[1]);
					break;
				}
			}
		}
		UserProfile userProfile = new UserProfile(user);
		if(lps.getMethodName().equals("addProduct") || lps.getMethodName().equals("deleteProduct") ||lps.getMethodName().equals("updateProduct") ) {
			userProfile.getWritingOperations().add(lps.getMethodName());
		}
		// tout opération nécessite une vérification d'existance don une lecture aussi
		userProfile.getReadingOperations().add(lps.getMethodName());
		
		String idOfSearchedProduct;
		Product tempProduct =null;
		List<String> idsOfExpensiveProducts = getIdsOfExpensiveProducts(products);
		// traitement pour obtenir les produits les plus chères rechercher
		if(lps.getMethodName().equals("fetchProduct")) {
			idOfSearchedProduct = getIndOfProductFromMsg(lps.getMsg());
			if(idsOfExpensiveProducts.contains(idOfSearchedProduct)) {
				tempProduct = getProductById(idOfSearchedProduct,products);
				userProfile.getMostExpensiveProductsSearched().add(tempProduct);
			}
		}
		
		
		return userProfile;
	}
}
