package userProfile;

public class LPSPersonalised {
		private String methodName;
		private String msg;
		
		
		
		public LPSPersonalised() {
			super();
			// TODO Auto-generated constructor stub
		}

		public LPSPersonalised(String methodName, String msg) {
			super();
			this.methodName = methodName;
			this.msg = msg;
		}

		public String getMethodName() {
			return methodName;
		}

		public void setMethodName(String methodName) {
			this.methodName = methodName;
		}

		public String getMsg() {
			return msg;
		}

		public void setMsg(String msg) {
			this.msg = msg;
		}
		
		
		
}
