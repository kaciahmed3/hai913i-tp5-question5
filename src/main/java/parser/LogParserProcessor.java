package parser;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;
import java.util.List;

import userProfile.LPSPersonalised;

public class LogParserProcessor {
	
	public List<LPSPersonalised> parseData(String inputFile) throws IOException {
	
		List<LPSPersonalised> lpsPersonalisedList = new LinkedList<LPSPersonalised>();
		
		InputStreamReader iReader = new InputStreamReader(new FileInputStream(inputFile), "UTF-8");
		BufferedReader bufferedReader = new BufferedReader(iReader);
		String line;
		String methodName=null;
		String msg=null;
		while ((line = bufferedReader.readLine()) != null) {
			if(line.contains("<method>")) {
				methodName = extractInfo(line);
			}else {
				if(line.contains("<message>")) {
					msg = extractInfo(line);
					lpsPersonalisedList.add(new LPSPersonalised(methodName, msg));
				}
				
			}
		}
		bufferedReader.close();
		iReader.close();
		return lpsPersonalisedList;

	}

	private String extractInfo(String line) {
		String tab1[]=line.split(">");
		String tab2[]=tab1[1].split("<");
		return tab2[0];
	}

}
