package ahmed_yanis.userProfilesProject;

import java.io.IOException;
import java.util.List;

import metrics.BestExpensiveUserProfile;
import metrics.BestReadingUserProfiles;
import metrics.BestWritingUserProfiles;
import parser.LogParserProcessor;
import products.Product;
import products.ProductProcessor;
import userProfile.LPSPersonalised;
import userProfile.UserProfile;
import userProfile.UserProfileProcessor;

public class App {
	private static String FileName = "";

	public static void main(String[] args) {
		FileName = args[0];
		LogParserProcessor parser = new LogParserProcessor();
		try {
			List<LPSPersonalised> lpsPersonnalisedlist = parser.parseData(FileName);
			ProductProcessor pp = new ProductProcessor();
			List<Product>products = pp.getAllProductSorted(lpsPersonnalisedlist);
			UserProfileProcessor userProfileProcessor = new UserProfileProcessor();
			List<UserProfile>userProfiles = userProfileProcessor.createAllUsersProfiles(lpsPersonnalisedlist, products);
			
			BestReadingUserProfiles bestReadingUserProfiles = new BestReadingUserProfiles();
			UserProfile bestRdUserProfile= 	bestReadingUserProfiles.getUserProfileWhithBestNumbreOfReadingOperations(userProfiles);
			bestReadingUserProfiles.displayUserProfileWhithBestNumbreOfReadingOperations(bestRdUserProfile);
			
			BestWritingUserProfiles bestWrintingUserProfiles = new BestWritingUserProfiles();
			UserProfile bestWtUserProfile= 	bestWrintingUserProfiles.getUserProfileWhithBestNumbreOfWritingOperations(userProfiles);
			bestWrintingUserProfiles.displayUserProfileWhithBestNumbreOfWritingOperations(bestWtUserProfile);
			
			BestExpensiveUserProfile bestExpensiveUserProfile = new BestExpensiveUserProfile();
			UserProfile bestScUserProfile= 	bestExpensiveUserProfile.getUserProfileExpensiveSearchingOperations(userProfiles);
			bestExpensiveUserProfile.displayUserProfileExpensiveSearchingOperations(bestScUserProfile);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
