package comparator;

import java.util.Comparator;

import products.Product;

public class ProductComparator implements Comparator<Product> {

	@Override
	public int compare(Product o1, Product o2) {
		Float myPrice = Float.valueOf(o1.getPrice());
		Float otherPrice = Float.valueOf(o2.getPrice());
		return otherPrice.compareTo(myPrice);
	}
	
	
}
