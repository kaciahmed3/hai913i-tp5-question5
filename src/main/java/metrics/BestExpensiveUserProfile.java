package metrics;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import products.Product;
import userProfile.UserProfile;

public class BestExpensiveUserProfile {
	
	public UserProfile getUserProfileExpensiveSearchingOperations(List<UserProfile>userProfiles) {
		int max =0;
		UserProfile userProfile=null;
		for (UserProfile userProfile2 : userProfiles) {
			if(userProfile2.getMostExpensiveProductsSearched().size() > max) {
				max=userProfile2.getMostExpensiveProductsSearched().size();
				userProfile = userProfile2;
			}
		}
		return userProfile;
	}
	
	public void displayUserProfileExpensiveSearchingOperations(UserProfile userProfile) {
		System.out.println("\n************** The userProfile with the best number of searching most expensives products operations ****************************\n");
		System.out.println(userProfile.getUser().toString());
		System.out.println("\nThe number of searching most expensives products operations is : "+userProfile.getMostExpensiveProductsSearched().size());
		System.out.println("\nList of most expensives products searched \n");
		Set<Product>productSet = new HashSet<Product>(userProfile.getMostExpensiveProductsSearched());
		for (Product p : productSet) {
			System.out.println("  "+p.toString());
		}
	}
}
