package metrics;

import java.util.List;

import userProfile.UserProfile;

public class BestWritingUserProfiles {
	
	public UserProfile getUserProfileWhithBestNumbreOfWritingOperations(List<UserProfile>userProfiles) {
		int max =0;
		UserProfile userProfile=null;
		for (UserProfile userProfile2 : userProfiles) {
			if(userProfile2.getWritingOperations().size() > max) {
				max=userProfile2.getWritingOperations().size();
				userProfile = userProfile2;
			}
		}
		return userProfile;
	}
	
	public void displayUserProfileWhithBestNumbreOfWritingOperations(UserProfile userProfile) {
		System.out.println("\n************** The userProfile with the best writing operations ****************************\n");
		System.out.println(userProfile.getUser().toString());
		System.out.println("\nThe number of writing operations is : "+userProfile.getWritingOperations().size());
		System.out.println("\nList of writing opeartions done \n");
		for (String op : userProfile.getWritingOperations()) {
			System.out.println("  "+op);
		}
	}

	
}
