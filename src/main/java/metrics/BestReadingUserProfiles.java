package metrics;

import java.util.List;

import userProfile.UserProfile;

public class BestReadingUserProfiles {
	
	public UserProfile getUserProfileWhithBestNumbreOfReadingOperations(List<UserProfile>userProfiles) {
		int max =0;
		UserProfile userProfile=null;
		for (UserProfile userProfile2 : userProfiles) {
			if(userProfile2.getReadingOperations().size()>max) {
				max=userProfile2.getReadingOperations().size();
				userProfile = userProfile2;
			}
		}
		return userProfile;
	}
	
	public void displayUserProfileWhithBestNumbreOfReadingOperations(UserProfile userProfile) {
		System.out.println("************** The userProfile with the best reading operations ****************************\n");
		System.out.println(userProfile.getUser().toString());
		System.out.println("\nThe number of reading operations is : "+userProfile.getReadingOperations().size());
		System.out.println("\nList of implicite and explict reading opeartions done \n");
		for (String op : userProfile.getReadingOperations()) {
			System.out.println("  "+op);
		}
	}

}
