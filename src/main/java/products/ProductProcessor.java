package products;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import comparator.ProductComparator;
import userProfile.LPSPersonalised;

public class ProductProcessor {
	
	public List<Product> getAllProductSorted(List<LPSPersonalised> lpsPersonaliseds){
		List<Product>products =createAllProducts(lpsPersonaliseds);
		Collections.sort(products, new ProductComparator());
		return products;
	}

	public List<Product> createAllProducts(List<LPSPersonalised> lpsPersonaliseds) {
		List<Product>products =new LinkedList<Product>();
		String tab[];
		Product p;
		for (LPSPersonalised lps : lpsPersonaliseds) {
			if (lps.getMsg().contains("Product")) {
				tab = lps.getMsg().split(";");
				p = createProduct(tab[1]);
				if(!containsProduct(products, p)) {
					products.add(p);
				}
			
			}
		}
		return products;
	}

	private Product createProduct(String stringProduct) {
		Product p = new Product();
		String tab1[] = stringProduct.split(",");
		String tab2[];
		for (int i = 0; i < tab1.length; i++) {
			tab2 = tab1[i].split("=");
			switch (i) {
				case 0: {
					p.setID(tab2[1]);
					break;
				}
				case 1: {
					p.setName(tab2[1]);
					break;
				}
				case 2: {
					p.setPrice(tab2[1]);
					break;
				}
				case 3: {
					p.setExpirationString(tab2[1]);
					break;
				}
			}
		}
		return p;

	}
	public boolean containsProduct(List<Product>lstp, Product p ) {
		for (Product product : lstp) {
			if(product.getID().equals(p.getID())) {
				return true;
			}
		}
		return false;
	}
}
