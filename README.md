# HAI913I TP5 Question5

Ce projet, est la suite de ce [projet](https://gitlab.com/yanisallouch/hai913i-tp5-spoon-logging). S'il vous plait, veuillez lire le [README](https://gitlab.com/yanisallouch/hai913i-tp5-spoon-logging/-/blob/main/README.md) de ce projet là.

##	Introduction to software traceability with [Spoon](https://spoon.gforge.inria.fr/)

Dans ce projet, nous proposons une application permettant de parser un fichier de `.log` au format XML crée par l'API [***Java Logging API***](https://docs.oracle.com/javase/8/docs/api/java/util/logging/package-summary.html), voir ce [tutoriel](https://www.jmdoudoux.fr/java/dej/chap-logging.htm#logging-3).

Il est supposé la chose suivante :
- Avoir le fichier de log intitulé `Repository.log` produit par l'éxécution de différent scénario de [l'application de test](https://gitlab.com/yanisallouch/hai913i-tp5-projet-de-test) intrumenté par notre [application](https://gitlab.com/yanisallouch/hai913i-tp5-spoon-logging) utilisant [Spoon](https://spoon.gforge.inria.fr/).

Pour des raisons de simplicité. Nous fournissons un fichier de log servant d'exemple dans le dossier [`samples/`](./samples).

##	How to run ?

1. Passer en argument du main de la classe `ahmed_yanis.userProfilesProject.App` le fichier [`Repository.log`](./samples/Repository.log) a analyser.
2. Observer les résultats dans la sortie standard de l'application (console in Eclipse).

##	What is next ?

Thanks you, for reading this far !